# Note
This project moved to [GitHub](https://github.com/hasadna/open-bus).
You can still watch its old versions here.

# OpenBus

A project under OpenTrain @ [the public knowledge workshop](http://http://www.hasadna.org.il).

A project which uses realtime measurments from [mot.gov.il](http://he.mot.gov.il/) with SIRI protocol and statistics to provide reliable public transportation arrivals times.

## Contribute
If you wish to contribute make a pull request and our team will get to it.

Further information
matangrn@gmail.com